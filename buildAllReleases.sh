#!/usr/bin/env bash

VERSIONS=$(curl -fsSL http://downloads.dlang.org/releases/2.x/ | sed -nEe 's/.*<li><a href="\/releases.*?">([0-9]+\.[0-9]+\.[0-9]+)<\/a>.*/\1/gp' | tac)

for VERSION in $VERSIONS; do
	echo "Compiler version $VERSION"
	sed -i "s/COMPILER_VERSION=.*/COMPILER_VERSION=$VERSION/" Dockerfile

	docker build -t $IMAGE:$VERSION .
	docker run --rm -i -v$(pwd):/source $IMAGE:$VERSION dmd --help | head -n2
	docker push $IMAGE:$VERSION
done;