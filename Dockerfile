FROM debian:stretch-slim

ENV COMPILER=dmd             \
    COMPILER_VERSION=2.081.1

RUN echo ">>> Making apt non-interactive <<<"                          \
 && export DEBIAN_FRONTEND=noninteractive                              \
 && echo 'APT::Get::Assume-Yes "true";'                                \
      >> /etc/apt/apt.conf.d/99minidmd                                 \
 && echo 'DPkg::Options "--force-confnew";'                            \
      >> /etc/apt/apt.conf.d/99minidmd                                 \
 && echo ">>> Updating <<<"                                            \
 && apt-get update -qq                                                 \
 && echo ">>> Installing necessary dependencies <<<"                   \
 && apt-get install -y --no-install-recommends libssl1.1 libssl-dev    \
      curl xz-utils libc-dev gnupg gcc ca-certificates unzip           \
      libcurl4-openssl-dev 2>&1 > /dev/null                            \
 && echo ">>> Installing ${COMPILER} ${COMPILER_VERSION} <<<"          \
 && mkdir /dlang /source                                               \
 && curl -fsS https://dlang.org/install.sh                             \
      | bash -s install -p /dlang -a "${COMPILER}-${COMPILER_VERSION}" \
 && chmod -R 755 /dlang                                                \
 && echo ">>> Removing unnecessary dependencies <<<"                   \
 && apt-get purge --auto-remove -yqq curl gnupg xz-utils unzip         \
      2>&1 > /dev/null                                                 \
 && echo ">>> Cleaning up <<<"                                         \
 && rm -rf /var/lib/apt/lists /var/cache/apt/archives                  \
 && rm -rf /etc/apt/apt.conf.d/99minidmd                               \
 && rm -rf /dlang/${COMPILER}-${COMPILER_VERSION}/linux/bin32          \
 && rm -rf /dlang/${COMPILER}-${COMPILER_VERSION}/linux/lib32          \
 && rm -rf /dlang/${COMPILER}-${COMPILER_VERSION}/samples              \
 && rm -rf /dlang/${COMPILER}-${COMPILER_VERSION}/html                 \
 && rm -rf /dlang/${COMPILER}-${COMPILER_VERSION}/man                  \
 && rm -rf /dlang/${COMPILER}-${COMPILER_VERSION}/activate             \
 && rm -rf /dlang/${COMPILER}-${COMPILER_VERSION}/activate.fish        \
 && rm -rf /dlang/${COMPILER}-${COMPILER_VERSION}/README.TXT

ENV \
    PATH=/dlang/dub:/dlang/${COMPILER}-${COMPILER_VERSION}/linux/bin64:${PATH} \
    LD_LIBRARY_PATH=/dlang/${COMPILER}-${COMPILER_VERSION}/linux/lib64         \
    LIBRARY_PATH=/dlang/${COMPILER}-${COMPILER_VERSION}/linux/lib64

VOLUME /source
WORKDIR /source
